# docker-molecule-nonroot

This repository creates multiple docker images based on popular linux distributions like Alpine, Debian, Ubuntu, CentOS to make testing with molecule as easy as possible.

# Container images

All container images _except Alpine Linux_ comming with two flavours a **simple** and a **systemd**

The **simple** container images is updated and as an `ansible` user configured to as the default user, it has also sudo permissions with NOPASSWD set.
The **systemd** extends the **simple** image with systemd as the init system, to test ansible roles that needs to start systemd-services.


## Pull Container Images

URL to pull images from `registry.gitlab.com`

**Archlinux**

| Namespace                                      | Tags     |
| ---------------------------------------------- | -------- |
| `/dvonessen/docker-molecule-nonroot/archlinux` | `latest` |

**AlpineLinux**

| Namespace                                   | Tags |
| ------------------------------------------- | ---- |
| `/dvonessen/docker-molecule-nonroot/alpine` | `3`  |

**Debian**

| Namespace                                   | Tags     |
| ------------------------------------------- | -------- |
| `/dvonessen/docker-molecule-nonroot/debian` | `10` `9` |

**Ubuntu**

| Namespace                                   | Tags            |
| ------------------------------------------- | --------------- |
| `/dvonessen/docker-molecule-nonroot/ubuntu` | `20.04` `18.04` |

**CentOS**

| Namespace                                   | Tags        |
| ------------------------------------------- | ----------- |
| `/dvonessen/docker-molecule-nonroot/almalinux` | `9` `8` |
| `/dvonessen/docker-molecule-nonroot/rockylinux` | `9` `8` |
